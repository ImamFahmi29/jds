# User Documentation
```
1. git clone https://gitlab.com/ImamFahmi29/jds.git
2. cd jds
3. composer install
4. rename .env.example => .env
5. Database Configuration
    - open file .env
    - change : 
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=jds_db (according to the database created)
        DB_USERNAME=root
        DB_PASSWORD=
    - save file
6. php artisan key:generate
7. php artisan migrate
8. php artisan l5-swagger:generate
    - api documentation : http://{url}/api/v1/documentation
9. php artisan optimize
10. php artisan serve
11. project running in http://localhost:8000

```

## Docker Configuration
```
1. sudo docker-compose up -d
2. sudo docker-compose exec -T app composer install
3. sudo docker-compose exec -T app cp .env.example .env
4. sudo docker-compose exec -T app php artisan key:generate
5. sudo docker-compose exec -T app php artisan l5-swagger:generate
6. sudo docker-compose exec -T app php artisan migrate
7. sudo docker-compose exec -T app php artisan optimize

note : 
- server ubuntu 20.04 
- aws ec2 instance
- 13.251.81.20:8100

```