<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * @OA\POST(
     *     path="/api/v1/user/show",
     *     tags={"Auth App"},
     *     summary="Returns with body nik,role and password",
     *     description="Returns with body nik,role and password",
     *     operationId="user show",
     *     @OA\Parameter(
     *          name="nik",
     *          description="Masukan nik",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="role",
     *          description="Masukan role",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     * @OA\Parameter(
     *          name="password",
     *          description="Masukan password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

     /**
     * @OA\POST(
     *     path="/api/v1/user/create",
     *     tags={"Auth App"},
     *     summary="return response with body id,role,nik,jwt",
     *     description="return response with body id,role,nik,jwt",
     *     operationId="user show",
     *     @OA\Parameter(
     *          name="nik",
     *          description="Masukan nik",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="role",
     *          description="Masukan role",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *      @OA\Parameter(
     *          name="password",
     *          description="Masukan password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

     /**
     * @OA\GET(
     *     path="/api/v1/user/check",
     *     tags={"Auth App"},
     *     summary="return response jwt valid or invalid",
     *     description="return response jwt valid or invalid",
     *     operationId="check jwt user",
     *     @OA\Parameter(
     *          name="access-token",
     *          required=true,
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation",
     *         @OA\JsonContent(),
     *     )
     * )
     */

    public function show(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'nik' => 'required|numeric|digits_between:16,16',
            'role' => 'required',
            'password' => 'required|min:6'
        ],[
            'required' => ':attribute wajib diisi',
            'digits_between' => ':attribute wajib 16 Digit Angka',
            'numeric' => ':attribute wajib numerik',
            'min' => ':attribute minimal :min karakter/digit',
            'max' => ':attribute maksimal :max karakter/digit',
        ],[
            'nik' => 'NIK',
            'role' => 'Role',
            'password' => 'Password',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors());
        }

        $data = [
            'nik' => $request->nik,
            'role' => $request->role,
            'password' => bcrypt($request->password),
        ];
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'nik' => 'required|numeric|digits_between:16,16|unique:users,nik',
            'role' => 'required',
            'password' => 'required|min:6'
        ],[
            'required' => ':attribute wajib diisi',
            'digits_between' => ':attribute wajib 16 Digit Angka',
            'numeric' => ':attribute wajib numerik',
            'min' => ':attribute minimal :min karakter/digit',
            'max' => ':attribute maksimal :max karakter/digit',
            'unique' => ':attribute sudah digunakan',
        ],[
            'nik' => 'NIK',
            'role' => 'Role',
            'password' => 'Password',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors());
        }

        try{
            $data = [
                'nik' => $request->nik,
                'role' => str_replace('_',' ',strtolower($request->role)),
                'password' => bcrypt($request->password),
                'api_token' => \Str::random(100)
            ];
    
            $result = User::create($data);
            $response = [
                'id' => $result->id,
                'nik' => $result->nik,
                'role' => $result->role,
                'jwt' => $result->api_token,
            ];

            return response()->json($response);
        }catch(\QueryBuilder $e){
            return response()->json(['message' => 'Opps..data gagal disimpan']);
        }
        

    }

    public function checking(Request $request)
    {
        return response()->json([
            'status' => true,
            'message' => 'JWT is valid',
            'jwt' => $request->header('access-token')
        ]);
    }
}
