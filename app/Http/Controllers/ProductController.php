<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @OA\GET(
     *     path="/api/v1/product/fetch",
     *     tags={"Fetch App"},
     *     summary="return with body add price IDR",
     *     description="return with body add price IDR",
     *     operationId="add IDR",
     *     @OA\Parameter(
     *          name="access-token",
     *          required=true,
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation",
     *         @OA\JsonContent(),
     *     )
     * )
     */

     /**
     * @OA\GET(
     *     path="/api/v1/product/aggregate",
     *     tags={"Fetch App"},
     *     summary="return aggregate orderby price IDR",
     *     description="return aggregate orderby price IDR",
     *     operationId="Aggregate Data",
     *     @OA\Parameter(
     *          name="access-token",
     *          required=true,
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation",
     *         @OA\JsonContent(),
     *     )
     * )
     */

    public $response=[];

    public function fetchData()
    {
        $products = $this->getProducts();
        $currency = $this->convertCurrency();

        if($products){
            $this->response[] = $this->productsData($products,$currency);
        }

        return response()->json($this->response);
    }

    public function aggregateData()
    {
        $response = [];
        $user = User::where('api_token',request()->header('access-token'))->first();

        $products = $this->getProducts();
        $currency = $this->convertCurrency();

        if($products && $user->isAdmin()){
            $results = $this->productsData($products,$currency);

            foreach ($results as $key => $row) {
                $response[$key]['IDR'] = $row['IDR'];
            }

            array_multisort($response,SORT_ASC);

        }
    
        return response()->json($response);
    }

    public function productsData($products,$currency)
    {

        foreach($products as $result){
            $this->response[]= [
                'id' => $result->id,
                'createdAt' => $result->createdAt,
                'department' => $result->department,
                'product' => $result->product,
                'price' => $result->price,
                'IDR' => $result->price * $currency 
            ];
        }

        return $this->response;
    }

    public function convertCurrency()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=1d5f665e36daa4815b7d",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return json_decode($response)->USD_IDR;
        }
    }

    public function getProducts()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://60c18de74f7e880017dbfd51.mockapi.io/api/v1/jabar-digital-services/product",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return json_decode($response);
        }

    }
}
