<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;

class AuthenticateApiV1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $users = User::get()->pluck('api_token')->toArray();
        
        if ($request->header('access-token')) {
            $api_key = $request->header('access-token');

            // check token
            if (in_array($api_key, $users)) {
                return $next($request);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'JWT is invalid',
                ]);
            }
        }

        return response()->json([
            'status' => false,
            'message' => 'Header Not Found',
        ]);
    }
}
