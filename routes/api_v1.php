<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user/show',[UserController::class,'show'])->name('user.v1.show');
Route::post('user/create',[UserController::class,'create'])->name('user.v1.create');
Route::get('user/check',[UserController::class,'checking'])->middleware('AuthApi')->name('user.v1.check');

Route::get('product/fetch',[ProductController::class,'fetchData'])->middleware('AuthApi')->name('product.v1.fetch');
Route::get('product/aggregate',[ProductController::class,'aggregateData'])->middleware('AuthApi')->name('product.v1.aggregate');